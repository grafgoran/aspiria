<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::resource('btc', 'BtcController');
Route::resource('eth', 'EthController');
Route::resource('xrp', 'XrpController');

Route::resource('btc', 'BtcController', ['middleware' => 'cors']);

Route::get('/btcDay', 'BtcController@getBtcByDay');
Route::get('/btcMonth', 'BtcController@getBtcByMonth');
Route::get('/btcYear', 'BtcController@getBtcByYear');

Route::get('/ethDay', 'EthController@getEthByDay');
Route::get('/ethMonth', 'EthController@getEthByMonth');
Route::get('/ethYear', 'EthController@getEthByYear');

Route::get('/xrpDay', 'XrpController@getXrpByDay');
Route::get('/xrpMonth', 'XrpController@getXrpByMonth');
Route::get('/xrpYear', 'XrpController@getXrpByYear');