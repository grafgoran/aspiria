<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXrp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xrps', function($table){
            $table->dateTime('date');
            $table->string('symbol');
            $table->float('open');
            $table->float('high');
            $table->float('low');
            $table->float('close');
            $table->float('volume_from');
            $table->float('volume_to');
            $table->increments('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('xrps');
    }
}
