<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use App\Xrp;
use Validator;
use Carbon\Carbon;

class XrpController extends APIBaseController
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    	$data = Xrp::all();
    	return $this->sendResponse($data->toArray(), 'Data retrieved successfully.');
    }

    public function getXrpByDay() {
        $data = Xrp::where('date', '>=', Carbon::now()->subDays(1))->OrderBy('date')->get(); 
        return $this->sendResponse($data->toArray(), 'Day: Data retrieved successfully.');
    }
    public function getXrpByMonth() {
        $data = Xrp::where('date', '>=', Carbon::now()->subDays(31))->OrderBy('date')->get(); 
        return $this->sendResponse($data->toArray(), 'Month: Data retrieved successfully.');
    }
    public function getXrpByYear() {
        $data = Xrp::where('date', '>=', Carbon::now()->subDays(365))->OrderBy('date')->get(); 
        return $this->sendResponse($data->toArray(), 'Year: Data retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'date' => 'required',
            'symbol' => 'required',
            'open' => 'required',
            'high' => 'required',
            'low' => 'required',
            'close' => 'required',
            'volume_from' => 'required',
            'volume_to' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }


        $data = Xrp::create($input);


        return $this->sendResponse($data->toArray(), ' created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Xrp::find($id);


        if (is_null($data)) {
            return $this->sendError('Data not found.');
        }


        return $this->sendResponse($data->toArray(), 'Data retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'date' => 'required',
            'symbol' => 'required',
            'open' => 'required',
            'high' => 'required',
            'low' => 'required',
            'close' => 'required',
            'volume_from' => 'required',
            'volume_to' => 'required'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }


        $data = Xrp::find($id);
        if (is_null($data)) {
            return $this->sendError('Data not found.');
        }


        //$data->name = $input['name'];
        //$post->description = $input['description'];

        $data->date = $input['date'];
        $data->symbol = $input['symbol'];
        $data->open = $input['open'];
        $data->high = $input['high'];
        $data->low = $input['low'];
        $data->close = $input['close'];
        $data->volume_from = $input['volume_from'];
        $data->volume_to = $input['volume_to'];

        $data->save();


        return $this->sendResponse($data->toArray(), 'Data updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Xrp::find($id);


        if (is_null($data)) {
            return $this->sendError('Data not found.');
        }


        $data->delete();


        return $this->sendResponse($id, 'Data deleted successfully.');
    }
}