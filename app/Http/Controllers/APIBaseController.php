<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIBaseController extends Controller
{
    public function sendResponse($result, $message) {

    	$response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200)->header('Access-Control-Allow-Origin', '*');
    }


    public function sendError($error, $errorMessages = [], $code = 404) {
    	
    	$response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
}