<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Xrp extends Model {

	protected $fillable = [
		'date', 'symbol', 'open', 'high', 'low', 'close', 'volume_from', 'volume_to',
	];
}